import os
import boto3

ECS_CLIENT = boto3.client("ecs", region_name=os.environ['AWS_DEFAULT_REGION'])


def register_ecs_task_definition(family, version):
    print("updating ecs task definition of {} to version {}".format(family, version))
    # Get existing task definition
    response = ECS_CLIENT.describe_task_definition(taskDefinition=family)
    taskDefinition = response["taskDefinition"]
    # update image to the last version
    containerDefinition = taskDefinition["containerDefinitions"][0]
    old_image = containerDefinition["image"]
    new_image = old_image.split(':')[0] + ":" + version
    containerDefinition["image"] = new_image
    # Register the new task definition
    response = ECS_CLIENT.register_task_definition(
        family=taskDefinition["family"],
        taskRoleArn=taskDefinition["taskRoleArn"],
        executionRoleArn=taskDefinition["executionRoleArn"],
        networkMode=taskDefinition["networkMode"],
        requiresCompatibilities=taskDefinition["requiresCompatibilities"],
        cpu=taskDefinition["cpu"],
        memory=taskDefinition["memory"],
        containerDefinitions=[containerDefinition]
    )

    # update the service
    response = ECS_CLIENT.update_service(
        cluster=family,
        service=family,
        taskDefinition=family
    )


if __name__ == '__main__':
    register_ecs_task_definition(family="service2", version=os.environ['CI_COMMIT_SHORT_SHA'])
