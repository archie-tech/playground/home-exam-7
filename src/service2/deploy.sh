#!/bin/bash

# export CI_COMMIT_SHORT_SHA
# export aws_account=???
# export AWS_DEFAULT_REGION="eu-west-2"
export aws_profile=default
export service_name="service2"

echo "deploying ${aws_account}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${service_name}:$CI_COMMIT_SHORT_SHA"

#apt-get -y update
#apt-get -y install python3-pip
pip3 install boto3

python3 src/service2/deploy.py