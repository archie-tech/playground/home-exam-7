from flask import Flask, request, Response
from apscheduler.schedulers.background import BackgroundScheduler
import boto3
import json
import os
import time

app = Flask(__name__)
scheduler = BackgroundScheduler()

SQS_CLIENT = boto3.client("sqs", region_name=os.environ['region_name'])
S3_CLIENT = boto3.client("s3", region_name=os.environ['region_name'])


@app.route('/', methods=['GET'])
def health_check():
    return Response(status=200)


def process_messages():
    # Long poll for message on provided SQS queue
    response = SQS_CLIENT.receive_message(
        QueueUrl=os.environ['queue_url'],
        AttributeNames=[
            'SentTimestamp'
        ],
        MaxNumberOfMessages=1,
        MessageAttributeNames=[
            'All'
        ],
        WaitTimeSeconds=20
    )
    # get messages from queue
    if 'Messages' in response:
        print("processing {} messages from queue".format(len(response['Messages'])))
        for message in response['Messages']:
            body = message['Body']
            id = message['MessageId']
            # delete message
            receipt_handle = message['ReceiptHandle']
            SQS_CLIENT.delete_message(QueueUrl=os.environ['queue_url'], ReceiptHandle=receipt_handle)
            # upload to bucket
            S3_CLIENT.put_object(Body=bytearray(json.dumps(body), "utf-8"), Bucket=os.environ['bucket_id'], Key="{}.json".format(id))


scheduler.add_job(process_messages, 'interval', seconds=60)
scheduler.start()


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=os.getenv("flask_port", "80"))
