from flask import Flask, request, Response
import boto3
import json
import os

app = Flask(__name__)

SQS_CLIENT = boto3.client("sqs", region_name=os.environ['region_name'])


def get_app_token():
    return '$DJISA<$#45ex3RtYr'


@app.route('/', methods=['GET', 'POST'])
def hello():
    # health check
    if request.method == "GET":
         return Response(status=200)
    # get user input
    payload = request.json
    token = payload['token']
    timestream = payload['data']['email_timestream']
    # verify token
    app_token = get_app_token()
    if app_token != token:
        return Response(status=400)
    # verify email data is valid
    for k in ["email_subject", "email_sender", "email_timestream", "email_content"]:
        if k not in payload['data']:
            return Response(status=400)
    # publish to q
    response = SQS_CLIENT.send_message(QueueUrl=os.environ['queue_url'], MessageBody=json.dumps(payload))
    #TODO: check response from boto3 client
    return Response(status=200)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
