#!/bin/bash

# export CI_COMMIT_SHORT_SHA
# export aws_account=???
# export AWS_DEFAULT_REGION="eu-west-2"
export aws_profile=default
export service_name="service1"

# run this script from its directory
cd `dirname $0`

# build
docker build --rm --tag $service_name .

# tag
docker tag ${service_name}:latest ${aws_account}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${service_name}:$CI_COMMIT_SHORT_SHA

# get token (the authorization token is valid for 12 hours)
#aws_token=$(aws --profile $aws_profile --region $AWS_DEFAULT_REGION ecr get-login-password)

# login
docker login --username AWS --password $aws_token $aws_account.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com

# push
docker push ${aws_account}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${service_name}:$CI_COMMIT_SHORT_SHA
