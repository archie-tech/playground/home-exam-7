resource "aws_s3_bucket" "email" {
  bucket = "service2-hilel-exam"
  tags = {
    Name = lower("${var.application} ${var.customer} email")
  }
}

resource "aws_ecs_cluster" "service2" {
  name = "service2"
  tags = {
    Name = "${var.application} ${var.customer} service2"
  }
}

resource "aws_ecs_task_definition" "service2" {
  family                   = "service2"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.service2.arn
  task_role_arn            = aws_iam_role.service2.arn
  tags = {
    Name = "${var.application} ${var.customer} service2"
  }
  container_definitions = jsonencode(
    [
      {
        "image" : "${aws_ecr_repository.service2.repository_url}:latest",
        "cpu" : 256,
        "memory" : 512,
        "name" : "service2",
        "networkMode" : "awsvpc",
        "portMappings" : [
          {
            "containerPort" : 80
          }
        ],
        "environment" : [
          { "name" : "region_name", "value" : var.aws_region },
          { "name" : "queue_url", "value" : aws_sqs_queue.email.url },
          { "name" : "bucket_id", "value" : aws_s3_bucket.email.id }
        ],
        "logConfiguration" : {
          "logDriver" : "awslogs",
          "options" : {
            "awslogs-create-group" : "true",
            "awslogs-group" : "service2",
            "awslogs-region" : "eu-west-2",
            "awslogs-stream-prefix" : "stream-prefix-2"
          }
        }
      }
    ]
  )
}

resource "aws_ecs_service" "service2" {
  name            = "service2"
  cluster         = aws_ecs_cluster.service2.id
  task_definition = aws_ecs_task_definition.service2.arn
  desired_count   = 1
  launch_type     = "FARGATE"
  network_configuration {
    security_groups = [aws_security_group.service2.id]
    subnets         = aws_subnet.private.*.id
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.service2.id
    container_name   = "service2"
    container_port   = 80
  }
  depends_on = [aws_lb_listener.service2]
  tags = {
    Name = "${var.application} ${var.customer} service2"
  }
}

resource "aws_iam_role" "service2" {
  name = "service2"
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "",
          "Effect" : "Allow",
          "Principal" : {
            "Service" : "ecs-tasks.amazonaws.com"
          },
          "Action" : "sts:AssumeRole"
        }
      ]
    }
  )
}

resource "aws_iam_policy" "service2" {
  name = lower("${var.application}-${var.customer}-service2")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "sqs:*"
          ],
          "Resource" : aws_sqs_queue.email.arn
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "s3:*"
          ],
          "Resource" : [
            aws_s3_bucket.email.arn,
            "${aws_s3_bucket.email.arn}/*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "ecr:*"
          ],
          "Resource" : [
            aws_ecr_repository.service2.arn,
            "${aws_ecr_repository.service2.arn}/*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "logs:*"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "service2" {
  role       = aws_iam_role.service2.name
  policy_arn = aws_iam_policy.service2.arn
}

resource "aws_iam_role_policy_attachment" "service2_ECSTaskExecution" {
  role       = aws_iam_role.service2.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}



resource "aws_security_group" "service2" {
  name   = "service2"
  vpc_id = aws_vpc.main.id
  ingress {
    protocol        = "tcp"
    from_port       = 80
    to_port         = 80
    security_groups = [aws_security_group.service2_lb.id]
  }
  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.application} ${var.customer} service2"
  }
}

