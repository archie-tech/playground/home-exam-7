resource "aws_sqs_queue" "email" {
  name = lower("${var.application}-${var.customer}-email")
  tags = {
    Name = lower("${var.application} ${var.customer} email")
  }
}

resource "aws_ecs_cluster" "service1" {
  name = "service1"
  tags = {
    Name = "${var.application} ${var.customer} service1"
  }
}

resource "aws_ecs_task_definition" "service1" {
  family                   = "service1"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.service1.arn
  task_role_arn            = aws_iam_role.service1.arn
  tags = {
    Name = "${var.application} ${var.customer} service1"
  }
  container_definitions = jsonencode(
    [
      {
        "image" : "${aws_ecr_repository.service1.repository_url}:latest",
        "cpu" : 256,
        "memory" : 512,
        "name" : "service1",
        "networkMode" : "awsvpc",
        "portMappings" : [
          {
            "containerPort" : 80
          }
        ],
        "environment" : [
          { "name" : "queue_url", "value" : aws_sqs_queue.email.url },
          { "name" : "region_name", "value" : var.aws_region }
        ],
        "logConfiguration" : {
          "logDriver" : "awslogs",
          "options" : {
            "awslogs-create-group" : "true",
            "awslogs-group" : "service1",
            "awslogs-region" : "eu-west-2",
            "awslogs-stream-prefix" : "stream-prefix-1"
          }
        }
      }
    ]
  )
}

resource "aws_ecs_service" "service1" {
  name            = "service1"
  cluster         = aws_ecs_cluster.service1.id
  task_definition = aws_ecs_task_definition.service1.arn
  desired_count   = 2
  launch_type     = "FARGATE"
  network_configuration {
    security_groups = [aws_security_group.service1.id]
    subnets         = aws_subnet.private.*.id
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.webapp.id
    container_name   = "service1"
    container_port   = 80
  }
  depends_on = [aws_lb_listener.webapp]
  tags = {
    Name = "${var.application} ${var.customer}"
  }
}

resource "aws_iam_role" "service1" {
  name = "service1"
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "",
          "Effect" : "Allow",
          "Principal" : {
            "Service" : "ecs-tasks.amazonaws.com"
          },
          "Action" : "sts:AssumeRole"
        }
      ]
    }
  )
}

resource "aws_iam_policy" "service1" {
  name = lower("${var.application}-${var.customer}-service1")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "sqs:GetQueueAttributes",
            "sqs:GetQueueUrl",
            "sqs:SendMessage",
            "sqs:SendMessageBatch"
          ],
          "Resource" : aws_sqs_queue.email.arn
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "ecr:*"
          ],
          "Resource" : [
            aws_ecr_repository.service1.arn,
            "${aws_ecr_repository.service1.arn}/*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "logs:*"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "service1" {
  role       = aws_iam_role.service1.name
  policy_arn = aws_iam_policy.service1.arn
}

resource "aws_iam_role_policy_attachment" "service1_ECSTaskExecution" {
  role       = aws_iam_role.service1.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}


resource "aws_security_group" "service1" {
  name   = "service1"
  vpc_id = aws_vpc.main.id
  ingress {
    protocol        = "tcp"
    from_port       = 80
    to_port         = 80
    security_groups = [aws_security_group.webapp_lb.id]
  }
  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.application} ${var.customer} service1"
  }
}

