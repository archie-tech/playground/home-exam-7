resource "aws_lb" "webapp" {
  name            = "webapp"
  subnets         = aws_subnet.public.*.id
  security_groups = [aws_security_group.webapp_lb.id]
  tags = {
    Name = "${var.application} ${var.customer}"
  }
}

resource "aws_lb_target_group" "webapp" {
  name        = "webapp"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"
  tags = {
    Name = "${var.application} ${var.customer}"
  }
}

resource "aws_lb_listener" "webapp" {
  load_balancer_arn = aws_lb.webapp.id
  port              = "80"
  protocol          = "HTTP"
  default_action {
    target_group_arn = aws_lb_target_group.webapp.id
    type             = "forward"
  }
  tags = {
    Name = "${var.application} ${var.customer}"
  }
}

resource "aws_security_group" "webapp_lb" {
  name   = "webapp_lb"
  vpc_id = aws_vpc.main.id
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.application} ${var.customer}"
  }
}

output "webapp_lb_dns_name" {
  value = aws_lb.webapp.dns_name
}
