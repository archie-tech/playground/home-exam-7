resource "aws_lb" "service2" {
  name               = "service2"
  load_balancer_type = "application" # defaults to "application"
  internal           = true      # defaults to false
  subnets            = aws_subnet.private.*.id
  security_groups    = [aws_security_group.service2_lb.id]
  tags = {
    Name = "${var.application} ${var.customer} service2"
  }
}

resource "aws_lb_target_group" "service2" {
  name        = "service2"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"
  tags = {
    Name = "${var.application} ${var.customer} service2"
  }
}

resource "aws_lb_listener" "service2" {
  load_balancer_arn = aws_lb.service2.id
  port              = "80"
  protocol          = "HTTP"
  default_action {
    target_group_arn = aws_lb_target_group.service2.id
    type             = "forward"
  }
  tags = {
    Name = "${var.application} ${var.customer} service2"
  }
}

resource "aws_security_group" "service2_lb" {
  name   = "service2_lb"
  vpc_id = aws_vpc.main.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.application} ${var.customer} service2"
  }
}
