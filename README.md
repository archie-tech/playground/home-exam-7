# Microservices Home Exam

## Overview

In this exercise, we use Terraform IaC to deploy to AWS an environment with the following components:

* 2 microservices, implemented as ECS services. Each one contains:
  * ECS cluster
  * ECS service
  * ECS task definition
  * Application Load Balancer
* SQS queue
* S3 Bucket

We also use GitLab to store the code and GitLab CI (SaaS, free tier) to run CI/CD pipelines for those 2 services.
The logic implemented by these components is described in the requirments file.

![Architecture Overview](docs/components.png)

Pink indicates components in a private subnet.

## Setup

* To recreate the environment, open a terminal inside infrastructure directory and run `terraform apply`.
* GitLab project requires the credentials of an IAM user, with permissions to perform tasks related to ECR and ECS. See remarks in file .gitlab-ci.yml for more details about pre-defined variables.
 
## Notes

* The deployment stage use Git commit hash as the version to deploy, and deployes the container automatically on every push. In a real environment, we will use better logic for that (e.g: use branch name as base for release version).

* Before pushing an image to ECR, we need to login using AWS CLI and get a token. In this exercise, we use a token from my local work-station as a GitLab CI project variable. This is a work-around to avoid installing AWS CLI on the shared GitLab runner. In real life, we will use a dedicated GitLab runner, with all the required dependendies pre-installed.

* Some IAM role policies (defined in the Terraform folder) are too permissive. This is done to speed-up the development, but before deploying to test and production evnironment we need to harden the policies and implement Tte principle of least privilege.